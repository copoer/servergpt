import { ChatGPTAPI } from 'chatgpt'
import { exec } from 'child_process'
import  fs from 'fs'
const logFile = Math.floor(new Date().getTime() / 1000) + '.log'
const api = new ChatGPTAPI({ apiKey: process.env.OPENAI_API_KEY })

const initCommand = `
From now on you will be interacting directly with a bash terminal on a fresh install of Ubuntu 20.04 64 Bit. This computer has an active connection to the internet. All of your output will be only terminal commands. Never explain yourself. All of the input you will be give will be the results of the terminal commands you outputed. You will start out as the root user. You must resolve issues if they occur using terminal command and never explain. You will complete a task by outputing a single terminal command at a time then wait for the terminal response from me.
Your task is to do the following only using one terminal command at a time and wait for the terminals response from me after each command:
Build a shopping website called wamazon. The shopping website will have a variety of products. Each product will have their own image and description. The website will have a home page where users can search through the products using a search bar that filters the elements on the page. Each product on the page will have a link to a more detailed page explaining the product.
The website will be hosted on the computer and must be visible to the outside world. The website will have a very modern design will well formatted pages and consistent styling. In the top left corner of each page the websites name will be written. At the bottom of each page will be an active page view count, the page view count will be stored locally on a postgres server and updated everytime some views a given page.
The website will be hosted on a nodejs server. The backend will be nodejs scripts that interact with a postgres server`;

(async function loop(res) {
	fs.appendFileSync(logFile, '\nchatGPT:\n' + res.text + '\n\n');
	exec(res.text, async (err, stdout, _) => {
		if (err) console.log(err)
		fs.appendFileSync(logFile, '\nTerminal:\n' + stdout + '\n\n');
		loop(await api.sendMessage(stdout, {
			conversationId: res.conversationId,
			parentMessageId: res.id
		}));
	});
})(await api.sendMessage(initCommand))
